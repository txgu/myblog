import sys, os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), 'site-packages.zip'))

from myblog import app as application
