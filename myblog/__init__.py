from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
import os



SECRET_KEY =  os.environ.get('SECRET_KEY', 'a secret key')

SITE_NAME = u"\u6653"

PAGE_SIZE = 10

# Mon, Sep 20, 2015
DATETIME_FORMAT="%a, %b %d, %Y"


# Local
DATABASES = {
    'NAME':     os.environ.get('MYSQL_ENV_DB_NAME', 'myblog'),
    'USER':     os.environ.get('MYSQL_ENV_DB_USER', 'root'),
    'PASSWORD': os.environ.get('MYSQL_ENV_DB_PASS', 'xx112358'),
    'HOST':     os.environ.get('MYSQL_PORT_3306_TCP_ADDR', 'localhost'),
    'PORT': '3306',
}

SQLALCHEMY_DATABASE_URI = 'mysql://{USER}:{PASSWORD}@{HOST}:{PORT}/{NAME}?charset=utf8'.format(**DATABASES)
SQLALCHEMY_POOL_RECYCLE = 10

DEBUG = True

MYBLOG_DIR = os.path.dirname(
    os.path.sep.join ( os.path.abspath(__file__).split(os.path.sep)[:-1] )
)

app = Flask(__name__,
    template_folder = os.path.join(MYBLOG_DIR , 'templates'),
    static_folder = os.path.join(MYBLOG_DIR, 'static'))

app.config.from_object(__name__)

#
db = SQLAlchemy(app)

import myblog.views, myblog.models
