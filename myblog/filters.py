from flask import g
from myblog import app

from datetime import datetime


def string_to_datetime(date_string):
    return datetime.strptime(date_string, app.config['DATETIME_FORMAT'])


def datetime_to_string(date):
    return date.strftime(app.config['DATETIME_FORMAT'])


def datetime_to_archive(date):
    return date.strftime("%b, %Y")







import markdown
from markdown.inlinepatterns import Pattern
from markdown import util
from markdown.util import etree
from flask import render_template_string


def markdown_to_html(content):
    if g.md is None:
        g.md = create_markdown()

    try:
        html = g.md.convert(content)
    except:
        return content
    else:
        return html


def create_markdown():
    md = markdown.Markdown(
            extensions = ['markdown.extensions.extra',
                    'markdown.extensions.admonition',
                    'markdown.extensions.codehilite',
                    'markdown.extensions.toc',
                    ],
            )

    makeJinjaExpressionPattern(md)

    return md

JINJA_EXPRESSION_RE = r'({{.+?(?<!})}})'


class JinjaExpressionPattern(Pattern):

    def __init__(self, pattern, md):
        Pattern.__init__(self, pattern, md)

    def handleMatch(self, m):
        jinja_block = m.group(2)

        try:
            # render returns a unicode object
            html = render_template_string(m.group(2))
        except Exception as e:
            return etree.fromstring('<em>Error: %s</em>' % m.group(2))

        try:
            return etree.fromstring(html)
        except:
            try:
                e = util.etree.Element(None)
            except:
                return html
            e.text = html
            return e

        #try:
        #    return etree.fromstring("<jinja>" + html + "</jinja>")
        #except Exception as e:
        #    return etree.fromstring('<em>Error: ' + str(e) + '</em>')


def makeJinjaExpressionPattern(md):
    md.inlinePatterns.add('jinja expression pattern', JinjaExpressionPattern(JINJA_EXPRESSION_RE, md), ">backtick") # after backtick



app.jinja_env.filters['stringtodatetime'] = string_to_datetime
app.jinja_env.filters['datetimetostring'] = datetime_to_string
app.jinja_env.filters['datetimetoarchive'] = datetime_to_archive
app.jinja_env.filters['markdowntohtml'] = markdown_to_html
