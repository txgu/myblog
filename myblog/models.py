from myblog import db, app

from sqlalchemy import func

from werkzeug.security import generate_password_hash, check_password_hash

from datetime import datetime, timedelta

tags = db.Table('tags',
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')),
    db.Column('post_id', db.Integer, db.ForeignKey('post.id'))
)

class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(32), unique=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Tag %r>' % (self.name,)

    @staticmethod
    def remove_empty():
        db.session.query(Tag).filter(~Tag.posts.any()).delete(synchronize_session='fetch')


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(32), unique=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Category %r>' % self.name


    @staticmethod
    def remove_empty():
        db.session.query(Category).filter(~Category.posts.any()).delete(synchronize_session='fetch')


class User(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), unique=True)
    showname = db.Column(db.String(32), unique=True)
    password = db.Column(db.String(128))

    def __init__(self, username, showname, password):
        self.username = username
        self.showname = showname
        self.password = generate_password_hash(password)


    def set_password(self, password):
        self.password = generate_password_hash(password)


    def check_password(self, password):
        return check_password_hash(self.password, password)


    def __repr__(self):
        return '<User %r@%r>' % (self.username, self.showname)




class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.String(256)) # title
    slug = db.Column(db.String(256)) # for url path, without any '/'
    publish_date = db.Column(db.DateTime) # date shown in page
    status = db.Column(db.String(32)) # status: draft or publish
    content = db.Column(db.Text) # uncompressed content

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User',
        backref=db.backref('posts', lazy='dynamic'))

    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship('Category',
        backref=db.backref('posts', lazy='dynamic'))

    tags = db.relationship('Tag', secondary=tags,
        backref=db.backref('posts', lazy='dynamic'))


    def __init__(self, user, title="", slug="", tags=[], category=None, status="draft", publish_date=datetime.now(), content=""):
        self.user = user
        self.title = title
        self.slug = slug
        self.tags = tags
        self.category = category
        self.status = status
        self.publish_date = publish_date
        self.content = content



    def __repr__(self):
        return '<Blog %r@%r>' % (self.user.username, self.title)


    @staticmethod
    def count(category=None, tag=None, archive=None, status=None):
        query = db.session.query(func.count(Post.id))

        if category:
            query = query.join(Post.category).filter(Category.name == category)

        if tag:
            query = query.join(Post.tags).filter(Tag.name == tag)

        if archive:
            begin, end = archive.date_range()
            query = query.filter(Post.publish_date.between(begin, end))

        if status:
            query = query.filter(Post.status == status)

        return query.scalar()


    @staticmethod
    def posts(page=None, category=None, tag=None, archive=None, status=None):
        query = db.session.query(Post)

        if category:
            query = query.join(Post.category).filter(Category.name == category)

        if tag:
            query = query.join(Post.tags).filter(Tag.name == tag)

        if archive:
            begin, end = archive.date_range()
            query = query.filter(Post.publish_date.between(begin, end))

        if status:
            query = query.filter(Post.status == status)

        if page:
           start, stop = page.ranges()
           return query.order_by(Post.publish_date.desc()).slice(start, stop).all()

        return query.order_by(Post.publish_date.desc()).all()


    @staticmethod
    def latest():
        return db.session.query(Post).filter(Post.status == 'publish').filter(Post.publish_date == db.session.query(func.max(Post.publish_date)).filter(Post.status == 'publish')).first()



class Page(object):

    def __init__(self, num, size=app.config['PAGE_SIZE']):
        self.num = num
        self.size = size


    def __repr__(self):
        return '<Page %d@%d>' % (num, size)


    def ranges(self):
        stop = self.num * self.size
        start = stop - self.size
        return start, stop


    def posts(self, all_posts):
        """Get all posts in a set
        """
        start, stop = self.ranges()
        return [post for index, post in enumerate(all_posts) if index < stop and index >= start]

    @staticmethod
    def create(posts):
        size = app.config['PAGE_SIZE']
        total = posts / size + 1

        return [Page(num, size) for num in range(1, total + 1)]


class Archive(object):

    def __init__(self, year, mon):
        self.date = datetime(year, mon, 1)


    def __repr__(self):
        return '<Archive %r>' % self.date


    def date_range(self):
        """Get all posts in this archive
        """
        min_date = self.date
        if min_date.month == 12:
            max_date = datetime(min_date.year + 1, 1, 1)
        else:
            max_date = datetime(min_date.year, min_date.month + 1, 1)

        # inclusive
        max_date = max_date - timedelta(1)

        return min_date, max_date


    @staticmethod
    def available():
        min_date = db.session.query(func.min(Post.publish_date)).scalar()
        max_date = db.session.query(func.max(Post.publish_date)).scalar()

        if not min_date:
            return []

        min_year = min_date.year
        min_mon  = min_date.month

        max_year = max_date.year
        max_mon  = max_date.month

        archives = []

        if min_year == max_year:
            for m in range(max_mon, min_mon - 1, -1):
                archives.append(Archive(min_year, m))

            return archives

        for m in range(max_mon, 0, -1):
            archives.append(Archive(max_year, m))


        for y in range(max_year, min_year - 1, -1):
            for m in range(12, 0, -1):
                archives.append(Archive(y, m))

        for m in range(12, min_mon - 1, -1):
            archives.append(Archive(min_year, m))

        return archives



class Breadcrumb(object):

    def __init__(self, name, url):
        self.name = name
        self.url = url

    def __repr__(self):
        return '<Breadcrumb %r@%r>' % (self.name, self.url)



class Site(object):

    @staticmethod
    def tags_to_string(tags):
        return ', '.join(set([tag.name for tag in tags]))


    @staticmethod
    def string_to_tag_names(tags):
        return set(filter(len, [tag.strip() for tag in tags.split(',')]))


    @staticmethod
    def archive_count(archive, status='publish'):
        return Post.count(archive=archive, status=status)


    @staticmethod
    def tag_count(tag, status='publish'):
        return Post.count(tag=tag, status=status)


    @staticmethod
    def category_count(category, status='publish'):
        return Post.count(category=category, status=status)

    @staticmethod
    def categories():
        return db.session.query(Category.name).all()

    @staticmethod
    def tags():
        return db.session.query(Tag.name).all()




app.jinja_env.globals.update(tags_to_string=Site.tags_to_string)
app.jinja_env.globals.update(string_to_tag_names=Site.string_to_tag_names)
app.jinja_env.globals.update(archive_count=Site.archive_count)
app.jinja_env.globals.update(tag_count=Site.tag_count)
app.jinja_env.globals.update(category_count=Site.category_count)
app.jinja_env.globals.update(categories=Site.categories)
app.jinja_env.globals.update(tags=Site.tags)
