from myblog import app
from sae.storage import Connection


def get_bucket():
    conn = Connection() # default parameters
    return conn.get_bucket('static') # default bucket

def list_dir():
    bucket = get_bucket()
    return bucket.list()


def put_file(file_name, content):
    bucket = get_bucket()
    bucket.put_object(file_name, content)


def get_file(file_name):
    bucket = get_bucket()
    return bucket.get(file_name)


def delete_file(file_name):
    bucket = get_bucket()
    bucket.delete_object(file_name)


def file_url(file_name):
    bucket = get_bucket()
    return bucket.generate_url(file_name)

def generate_image(file_name):
    return "<img src='%s' class='img-responsive'/>" % file_url(file_name)


app.jinja_env.globals.update(file_url=file_url)
app.jinja_env.globals.update(generate_image=generate_image)









