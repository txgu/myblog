from myblog import app, db
from flask import render_template, get_template_attribute, request, abort, make_response, session, g, redirect, flash, url_for

from myblog.models import *

from datetime import datetime

from myblog.filters import string_to_datetime, datetime_to_string, datetime_to_archive

from sqlalchemy import or_

import logging


def redirect_url(default='index'):
    return request.args.get('next') or \
           request.referrer or \
           url_for(default)

def pages(**kwargs):
    if g.user:
        return Page.create(Post.count(**kwargs))

    return Page.create(Post.count(**dict(kwargs, status='publish')))


def posts(page, **kwargs):
    if g.user:
        return Post.posts(page=page, **kwargs)

    return Post.posts(page=page, **dict(kwargs, status='publish'))


@app.before_request
def before_request():
    if 'username' in session:
        g.user = User.query.filter_by(username=session['username']).first()
    else:
        g.user = None

    g.breadcrumb = [Breadcrumb('Home', url_for('index'))]

    g.md = None


@app.teardown_request
def teardown_request(exception):
    # For sae
    db.session.close()


@app.route('/login', methods=['POST'])
def login():
    try:
        username = request.form.get('username', 'txgu')
        user = User.query.filter(User.username==username).first()

        if user is None:
            flash(u'User %s does not exist' % username, 'warning')
            return redirect(redirect_url())

        if not user.check_password(request.form.get('password', '')):
            flash(u'Incorrect password', 'warning')
            return redirect(redirect_url())

        session['username'] = username
        g.user = user
        return redirect(redirect_url())
    except Exception as e:
        logging.exception("Cannot logging!")

    abort(404)


@app.route('/logout', methods=['GET'])
def logout():
    session.pop('username')
    g.user = None
    flash(u'You have successfully logged out!', 'info')
    return redirect(url_for('index'))


def fill_post(post, form):
    post.title = form['title'].strip()
    post.slug  = form['slug'].strip()
    post.publish_date = string_to_datetime(form['publish_date'])
    post.status = form['status'].strip()

    category = Category.query.filter(Category.name == form['category']).first()

    if post.category:
        if post.category.name != form['category'].strip():
            post.category = category or Category(form['category'].strip())
            db.session.add(post.category)
    else:
        post.category = category or Category(form['category'].strip())
        db.session.add(post.category)


    tag_names = Site.string_to_tag_names(form['tags'].strip())

    # remove first
    post.tags = [tag for tag in post.tags if tag.name in tag_names]

    # then create and add
    for tag_name in tag_names:
        tag = Tag.query.filter(Tag.name == tag_name).first()
        if tag is None:
            tag = Tag(tag_name)
            db.session.add(tag)

        if tag not in post.tags:
            post.tags.append(tag)

    post.content = form['content']


def validate_post(post):
    error_msgs = []

    if post.title and len(post.title) == 0:
        error_msgs.append('Title is empty')

    if post.slug and len(post.slug) == 0:
        error_msgs.append('Slug is empty')


    if post.status not in ['publish', 'draft']:
        error_msgs.append('Unsupported status ' + post.status)


    for msg in error_msgs:
        flash(msg, 'warning')

    if len(error_msgs) > 0:
        raise RuntimeError('Validating post failed!')

    query = db.session.query(Post).filter(or_(Post.title == post.title, Post.slug == post.slug))

    if post.id:
        query = query.filter(Post.id != post.id)

    if query.first():
        raise RuntimeError('Either title or slug is duplicated!')


def update_post(post, form):
    fill_post(post, form)
    validate_post(post)

    db.session.add(post)
    db.session.commit()


@app.route('/new', methods=['GET', 'POST'])
def new():
    if g.user is None:
        flash(u'You should login!', 'warning')
        return redirect(redirect_url())

    if request.method == 'GET':
        post = Post(g.user, "", "", [], None, "draft", datetime.now(), "")
        g.breadcrumb.append(Breadcrumb('New', url_for('new')))
        return render_template('new.html', post=post)


    post = Post(g.user)

    try:
        update_post(post, request.form)
    except Exception as e:
        flash(u'Cannot create post, cause %s' % e, 'warning')
        g.breadcrumb.append(Breadcrumb('New', url_for('new')))
        return render_template('new.html', post=post)
    else:
        return redirect(url_for('post', slug=post.slug))


@app.route('/publish/<postid>', methods=['GET', 'POST'])
def publish(postid=None):
    if g.user is None:
        flash(u'Only logged user can publish', 'warning')
        return redirect(redirect_url())

    post = Post.query.filter(Post.id == postid).first()
    if not post:
        flash(u'No post with id' + postid, 'warning')
        return redirect(redirect_url())

    try:
        if post.status == 'draft':
            post.status = 'publish'
            db.session.add(post)
            db.session.commit()

            flash(u'Publish post %s' % (post.title), 'info')
        else:
            flash(u'Cannot publish post %s  with status %s' % (post.title, post.status), 'warning')

    except Exception as e:
        flash(u'Publish post %s with an exception %s' % (post.title, e), 'warning')
        return redirect(redirect_url())

    return redirect(redirect_url())


@app.route('/edit/<postid>', methods=['GET', 'POST'])
def edit(postid=None):
    if g.user is None:
        flash(u'You should login!', 'warning')
        return redirect(redirect_url())

    if request.method == 'GET':
        post = Post.query.filter(Post.id == postid).first()
        if not post:
            flash(u'No post with id' + postid, 'warning')
            return redirect(redirect_url())

        g.breadcrumb.append(Breadcrumb('<i class="glyphicon glyphicon-edit"></i> %s' % post.title, url_for('edit', postid=postid)))
        return render_template('edit.html', post=post)


    post = Post.query.filter(Post.id == postid).first()

    try:
        update_post(post, request.form)
    except Exception as e:
        flash(u'Cannot update post, cause %s' % e, 'warning')
        g.breadcrumb.append(Breadcrumb('<i class="glyphicon glyphicon-edit"></i> %s' % post.title, url_for('edit', postid=postid)))
        return render_template('edit.html', post=post)
    else:
        return redirect(url_for('post', slug=post.slug))



@app.route('/tag', methods=['GET'])
@app.route('/tag/<tag>', methods=['GET'])
def tag(tag=None):
    g.breadcrumb.append(Breadcrumb('Tag', url_for('tag')))
    if not tag:
        Tag.remove_empty()
        tags = Tag.query.all()
        return render_template('tags.html', tags=tags)

    num = request.args.get('p', 1)
    g.pages = pages(tag=tag)
    if num >= 1 and num <= len(g.pages):
        g.page = g.pages[num-1]
        g.posts = posts(g.page, tag=tag)
        g.breadcrumb.append(Breadcrumb(tag, url_for('tag', tag=tag)))
        return render_template('posts.html')

    return redirect(url_for('index'))



@app.route('/category', methods=['GET'])
@app.route('/category/<category>', methods=['GET'])
def category(category=None):
    g.breadcrumb.append(Breadcrumb('Category', url_for('category')))
    if not category:
        Category.remove_empty()
        categories = Category.query.all()
        return render_template('categories.html', categories=categories)

    num = request.args.get('p', 1)
    g.pages = pages(category=category)
    if num >= 1 and num <= len(g.pages):
        g.page = g.pages[num-1]
        g.posts = posts(g.page, category=category)
        g.breadcrumb.append(Breadcrumb(category, url_for('category', category=category)))
        return render_template('posts.html')

    return redirect(url_for('index'))


@app.route('/preview', methods=['POST'])
def preview():
    render = get_template_attribute('helper.html', 'render_preview_post')
    return make_response(render(request.form), 200)


@app.route('/archive', methods=['GET'])
@app.route('/archive/<int:year>/<int:mon>', methods=['GET'])
def archive(year=None, mon=None):
    g.breadcrumb.append(Breadcrumb('Archive', url_for('archive')))
    if not year:
        archives = Archive.available()
        return render_template('archives.html', archives=archives)

    if not mon:
        mon = 1

    num = request.args.get('p', 1)
    archive = Archive(year, mon)
    g.pages = pages(archive = archive)
    if num >= 1 and num <= len(g.pages):
        g.page = g.pages[num-1]
        g.posts = posts(g.page, archive=archive)

        g.breadcrumb.append(Breadcrumb(datetime_to_archive(archive.date), url_for('archive', year=year, mon=mon)))
        return render_template('posts.html')

    return redirect(url_for('index'))


@app.route('/post', methods=['GET'])
@app.route('/post/<slug>', methods=['GET'])
def post(slug=None):
    g.breadcrumb.append(Breadcrumb('Post', url_for('post')))
    if slug:
        post = Post.query.filter(Post.slug == slug).filter(Post.status == 'publish').first()
        if post:
            g.breadcrumb.append(Breadcrumb(post.title, url_for('post', slug=post.slug)))
            return render_template('post.html', post=post)

        flash(u'No post at ' + slug, 'warning')
        return redirect(url_for('post'))

    num = int(request.args.get('p', 1))
    g.pages = pages()
    if num >= 1 and num <= len(g.pages):
        g.page = g.pages[num-1]
        g.posts = posts(g.page)
        return render_template('posts.html')

    flash('Incorrect page number %d' % num, 'info')
    return redirect(url_for('post'))


@app.route('/')
def index():
    return render_template('index.html', post=Post.latest())


@app.route('/about')
def about():
    g.breadcrumb.append(Breadcrumb('About', url_for('about')))
    return render_template('about.html')


@app.route('/contact')
def contact():
    return render_template('index.html')



@app.route('/remove/<path>', methods=['GET'])
def remove(path):
    if g.user is None:
        flash(u'You should login!', 'warning')
        return redirect(redirect_url())

    #saeutils.delete_file(path)
    flash(u'You have removed ' + path, 'info')
    return redirect(redirect_url())


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if g.user is None:
        flash(u'You should login!', 'warning')
        return redirect(redirect_url())

    if request.method == 'GET':
        return render_template('upload.html', file_list=[])#saeutils.list_dir())

    for name, file in request.files.iteritems():
        pass
        #saeutils.put_file(file.filename, file.read())

    return redirect(redirect_url())
