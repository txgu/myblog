function previewPost(url) {
    if ($('#post-form').length == 0) {
        return;
    }

    var data = $('#post-form').serializeArray(); // convert form to array
    data.push({name: "content", value: editor.getValue()});

    $.ajax({
        type: "POST",
        url: url,
        data: $.param(data),
    }).done(function(data, textStatus, jqXHR) {
        $('#preview-dialog .modal-body').empty().append(data)
        $('#preview-dialog').modal('show');
    }).fail(function(jqXHR, textStatus, errorThrown) {
        $('#preview-dialog .modal-body').empty().append(jqXHR.responseText)
        $('#preview-dialog').modal('show');
    });
}


